var path = require('path');

module.exports = function(options = {}) {
	var db = require('./db')(options);

	async function clearDistDirectory() {
		let cwd = options.cwd || process.cwd()
		let distPath = path.join(cwd, options.distPath || 'dist')
		let rimrafPath = distPath + '/**/*.*';
		console.log('clearDistDirectory', {
			rimrafPath
		});
		await require('rmfr')(rimrafPath, {
			glob: true
		});
	}

	async function compilePages() {
		const pug = require('pug');
		const sander = require('sander');
		let cwd = options.cwd || process.cwd()
		let distPath = options.distPath || 'dist'
		let items = await db.pages.getAll();
		await Promise.all(items.map((item) => {

			console.log('Page written:', {
				item
			})

			let templatePath = `src/templates/${item.template}.pug`;
			templatePath = path.join(cwd, templatePath);
			const compileItem = pug.compileFile(templatePath, {});

			let html = compileItem(item)
			
			return sander.writeFile(path.join(cwd, distPath, `${item.path}.html`), html);
		}));
	}

	return {
		clearDistDirectory,
		compilePages
	}
}