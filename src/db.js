var scope = null;

function init(options) {
	if (!!scope) {
		return scope;
	}
	var mongoose = require('mongoose')
	var uri = options.dbUri || process.env.DB_URI;
	console.log(`db using uri ${uri}`)
	const mongooseOptions = {
		useNewUrlParser: true
	}
	var conn = mongoose.createConnection(uri, mongooseOptions);
	var createModels = require('./models/index');
	scope =  createModels(conn)
	return scope;
}

module.exports = function(options = {}) {
	return init(options);
}