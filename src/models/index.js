module.exports = function(conn) {
	return {
		pages: require('./pages')(conn),
		features: require('./features')(conn)
	};
}