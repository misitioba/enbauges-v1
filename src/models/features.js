var mongoose = require('mongoose')
var Schema = mongoose.Schema;
module.exports = function(conn) {

	let schema = new Schema({
		name: {
			type: String
		},
		code:{
			type:String,
			unique:true,
			required:true
		},
		description:String,
		options:{}
	}, {
		timestamps: true
	});

	var middlewares = {
		async beforeSave(data) {
			
		}
	};

	return require('./model').create('features', {
		conn,
		schema,
		middlewares
	});
};