var mongoose = require('mongoose')
var Schema = mongoose.Schema;
module.exports = function(conn) {

	let schema = new Schema({
		title: {
			type: String,
			required: true
		},
		name: {
			type: String,
			required: true
		},
		path: {
			type: String,
			required: true
		},
		html: String,
		template: {
			type: String,
			default: 'page',
			required: true
		},
		isArticle:{
			type:String,
			default:false
		},
		hashtags:{
			type:[String]
		},
		features:{
			type: [{
				type:mongoose.Schema.Types.ObjectId,
				ref: 'features'
			}],
			default:[]
		}
	}, {
		timestamps: true
	});

	schema.methods.addFeature = function(name, cb) {
    	var features = require('./').features;

  	};

	var middlewares = {
		async beforeSave(data) {
			const toCamel = (s) => {
				s = s.split(' ').join('-');
				return s.replace(/([-_][a-z])/ig, ($1) => {
					return $1.toUpperCase()
						.replace('-', '')
						.replace('_', '');
				});
			};
			var camelCaseToKebabCase = string => string.replace(/([a-z])([A-Z])/g, '$1-$2').toLowerCase();
			data.name = camelCaseToKebabCase(toCamel(data.title))
			//data.path = `/${data.name}`
			data.template = data.template || 'page'
		}
	};

	return require('./model').create('pages', {
		conn,
		schema,
		middlewares
	});
};

async function addFeature(data){

}