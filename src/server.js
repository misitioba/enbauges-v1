require('dotenv').config({
	silent: true
});
var express = require('express');
var app = express();
app.get('/', function(req, res) {
	res.send('Hello World!');
});
var path = require('path')
let cwd = '/home/javi/git/bauges';
if(process.env.NODE_ENV === 'production'){
	cwd = process.cwd();
}

let mainModule = require(path.join(cwd, 'src'));
let middlewareOptions = {
	cwd: cwd,
	distPath: `dist`,
	dbUri: process.env.DB_URI
};
mainModule.middleware(app, middlewareOptions);
mainModule.runTests(middlewareOptions);

let port = process.env.PORT || 3000;

var path = require('path');

app.use('/css', express.static('src/css'));
app.use('/js', express.static('src/js'));

app.get('/api/pages',async (req,res)=>{
	let pages = await app.db.pages.getModel().find({});
	res.json(pages);
});

app.get('/admin/*',(req,res)=>{
	const pug = require('pug');
	let appTemplate = path.join(process.cwd(), `src/templates/app.pug`);
	const compileItem = pug.compileFile(appTemplate, {});
	let html = compileItem({});
	res.send(html);
});

app.listen(port, function() {
	console.log(`Bauges running on port ${port}`);
});